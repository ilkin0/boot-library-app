package com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.impl;

import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.JwtService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.UserDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.mapper.UserMapperImpl;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.ApplicationUser;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.Authority;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.repo.UserRepo;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.impl.user.UserServiceImpl;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.mail.MailService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.token.TokenService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserServiceImplTest {

    @Mock
    private UserService userService;

    @Mock
    private UserRepo userRepo;

    @Mock
    private UserMapperImpl mapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private MailService mailService;

    @Mock
    private JwtService jwtService;

    @Mock
    private TokenService tokenService;

    @Mock
    private AuthenticationManagerBuilder authenticationManagerBuilder;

    private ApplicationUser user;
    private UserDto userDto;

    @BeforeEach
    void init() {

        mapper = new UserMapperImpl();
        passwordEncoder = mock(PasswordEncoder.class);
        userRepo = mock(UserRepo.class);
        tokenService = mock(TokenService.class);

        userService = new UserServiceImpl(userRepo, tokenService, mapper, passwordEncoder, mailService, jwtService, authenticationManagerBuilder);

        Authority authority = new Authority();
        authority.setName("USER");

        user = ApplicationUser.builder()
                .username("username")
                .email("email")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .password("password")
                .authorities(Set.of(authority))
                .verificationToken(VerificationToken.builder()
                        .token(UUID.randomUUID().toString())
                        .expiresAt(LocalDateTime.now().plus(4, ChronoUnit.HOURS))
                        .build())
                .build();

        user.setId(1L);

        userDto = mapper.toUserDto(user);
    }


    @Test
    public void signup_user_when_dto_valid() {
        when(userRepo.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
        when(userRepo.save(any())).thenReturn(user);

        userService.signUp(userDto);

        assertThat(user.getVerificationToken()).isNotNull();
        assertThat(user.getVerificationToken().getToken()).isNotNull();
    }

}
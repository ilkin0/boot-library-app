package com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.ApplicationUser;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken.TABLE_NAME;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = TABLE_NAME)
public class VerificationToken {
    public static final String TABLE_NAME = "verification_tokens";

    @Id
    @Column(name = "id", nullable = false, length = 10)
    private Long id;

    @Column(nullable = false)
    private String token;
    private LocalDateTime expiresAt;
    private LocalDateTime confirmedAt;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @MapsId
    @JoinColumn(name = "user_id")
    private ApplicationUser user;
}

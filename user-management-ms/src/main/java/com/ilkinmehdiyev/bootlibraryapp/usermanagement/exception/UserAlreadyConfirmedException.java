package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class UserAlreadyConfirmedException extends InvalidStateException {

    public UserAlreadyConfirmedException() {
        super("Email already confirmed");
    }
}

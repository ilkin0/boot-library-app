package com.ilkinmehdiyev.bootlibraryapp.usermanagement.controller.user;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.mapper.UserMapper;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.impl.user.UserServiceImpl;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.utils.Constants.USER_API_URL;

@Slf4j
@RestController
@RequestMapping(USER_API_URL)
public record UserController(UserService userService, UserMapper mapper) {

    @GetMapping(path = "/activation")
    public void verifyUser(@RequestParam("token") String token) {
        userService.activateUser(token);
    }
}

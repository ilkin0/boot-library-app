package com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.utils.Constants.PASSWORD_MIN_LENGTH;

public record LoginDto(
        @NotNull @Size(min = 1, max = 50) String email,
        @NotNull @Size(min = PASSWORD_MIN_LENGTH) String password,
        boolean rememberMe
) {
}

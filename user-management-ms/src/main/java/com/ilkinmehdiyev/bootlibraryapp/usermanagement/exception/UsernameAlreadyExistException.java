package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class UsernameAlreadyExistException extends InvalidStateException {

    public static final String USERNAME_ALREADY_EXIST = "Email \"%s\" already registered,try different email";

    public UsernameAlreadyExistException(String username) {
        super(String.format(USERNAME_ALREADY_EXIST, username));
    }
}

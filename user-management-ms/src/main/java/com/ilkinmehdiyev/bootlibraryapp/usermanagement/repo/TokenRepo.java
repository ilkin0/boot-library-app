package com.ilkinmehdiyev.bootlibraryapp.usermanagement.repo;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

public interface TokenRepo extends JpaRepository<VerificationToken, Long> {

    Optional<VerificationToken> findByToken(String token);

    @Transactional
    @Modifying
    @Query(
            """
                    UPDATE VerificationToken vtoken
                    SET vtoken.confirmedAt = :now
                    WHERE vtoken.token = :token
                    """
    )
    void updateTokenConfirmAt(String token, LocalDateTime now);
}

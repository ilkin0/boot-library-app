package com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto;

public record AccessTokenDto(String accessToken) {
}

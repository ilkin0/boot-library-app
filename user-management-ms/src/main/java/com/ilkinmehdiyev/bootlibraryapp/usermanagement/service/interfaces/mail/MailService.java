package com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.mail;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface MailService {
    void sendVerificationMail(String to, String verificationApi) throws MessagingException, UnsupportedEncodingException;

    String buildMail(String verificationApi);
}

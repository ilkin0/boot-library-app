package com.ilkinmehdiyev.bootlibraryapp.usermanagement.mapper;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.UserDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.ApplicationUser;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto toUserDto(ApplicationUser applicationUser);

    ApplicationUser toUser(UserDto userDto);
}

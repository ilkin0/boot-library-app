package com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto;

import com.ilkinmehdiyev.bootlibraryapp.common.security.validation.email.EmailConstraint;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.Authority;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
public class UserDto {

//    Change to Record class

    @NotNull
    private String name;

    @EmailConstraint
    private String email;

    private int age;

    private String username;

    private String avatar;

    private String password;

    private Set<Authority> authorities;

    private LocalDateTime creationDate = LocalDateTime.now();

    private VerificationToken verificationToken;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;
}

package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class UserNotActiveException extends InvalidStateException {

    public UserNotActiveException() {
        super("The user is not active. Please activate your account");
    }
}

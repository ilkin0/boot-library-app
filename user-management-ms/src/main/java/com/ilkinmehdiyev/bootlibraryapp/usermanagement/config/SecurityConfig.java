package com.ilkinmehdiyev.bootlibraryapp.usermanagement.config;

import com.ilkinmehdiyev.bootlibraryapp.common.security.auth.AuthenticationEntryPointConfigurer;
import com.ilkinmehdiyev.bootlibraryapp.common.security.config.BaseSecurityConfig;
import com.ilkinmehdiyev.bootlibraryapp.common.security.config.properties.SecurityProperties;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.impl.JwtServiceImpl;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.AuthService;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.JwtService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.utils.Constants.AUTH_API_URL;

@Configuration
@EnableWebSecurity
@Import({JwtServiceImpl.class, SecurityProperties.class, AuthenticationEntryPointConfigurer.class})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends BaseSecurityConfig {

    public SecurityConfig(List<AuthService> authServices) {
        super(authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(
                        AUTH_API_URL + "/sign-in",
//                        "/auth/forgot-password",
//                        "/user/password/forgot",
//                        "/user/password/reset",
                        "/api/v1/users/activation**"
                ).permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/users");

        super.configure(http);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

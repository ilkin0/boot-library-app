package com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.BaseEntity;
import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.Authority.TABLE_NAME;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = TABLE_NAME)
public class Authority extends BaseEntity
        implements GrantedAuthority {

    public static final String TABLE_NAME = "authorities";

    private String name;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "authorities")
    private Set<ApplicationUser> user;

    @Override
    public String getAuthority() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Authority authority = (Authority) o;
        return name != null && Objects.equals(name, authority.name);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
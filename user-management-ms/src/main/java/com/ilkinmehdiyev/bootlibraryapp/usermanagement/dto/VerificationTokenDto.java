package com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class VerificationTokenDto {
    private String token;

    private LocalDateTime expiresAt;

    private LocalDateTime confirmedAt;
}

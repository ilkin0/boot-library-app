package com.ilkinmehdiyev.bootlibraryapp.usermanagement.enums;

public enum UserAuthority {
    ANONYMOUS,
    USER,
    ADMIN
}

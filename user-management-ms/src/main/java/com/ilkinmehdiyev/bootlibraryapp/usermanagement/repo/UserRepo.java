package com.ilkinmehdiyev.bootlibraryapp.usermanagement.repo;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<ApplicationUser, Long> {

    Optional<ApplicationUser> findByUsername(String username);

    Optional<ApplicationUser> findByEmail(String email);

    Optional<ApplicationUser> findByVerificationToken(VerificationToken token);
}

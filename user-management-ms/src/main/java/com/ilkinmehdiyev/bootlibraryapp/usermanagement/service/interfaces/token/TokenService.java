package com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.token;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;

public interface TokenService {
    void saveVerificationToken(VerificationToken token);

    VerificationToken getVerificationToken(String token);

    void changeTokenConfirmedAt(String token);

    VerificationToken confirmUserToken(String token);
}

package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class TokenExpiredException extends InvalidStateException {

    public TokenExpiredException() {
        super("Token expired, please request new activation link");
    }
}

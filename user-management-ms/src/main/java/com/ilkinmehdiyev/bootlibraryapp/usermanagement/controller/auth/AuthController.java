package com.ilkinmehdiyev.bootlibraryapp.usermanagement.controller.auth;

import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.JwtService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.AccessTokenDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.LoginDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.UserDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.repo.UserRepo;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.auth.AuthService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.ilkinmehdiyev.bootlibraryapp.common.utils.HttpConstants.BEARER_AUTH_HEADER;
import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.utils.Constants.AUTH_API_URL;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
@RestController
@RequestMapping(AUTH_API_URL)
public record AuthController(
        UserService userService, UserRepo userRepo,
        AuthenticationManagerBuilder authenticationManagerBuilder, JwtService jwtService) {

    @PostMapping("/register")
    public void saveUser(@Valid @RequestBody UserDto userDto) {
        log.trace("Sign up request with email {}", userDto.getEmail());
        userService.signUp(userDto);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<AccessTokenDto> authorize(@Valid @RequestBody LoginDto loginDto) {

        String jwtToken = userService.signIn(loginDto);

        return ResponseEntity.ok()
                .header(AUTHORIZATION, BEARER_AUTH_HEADER + " " + jwtToken)
                .body(new AccessTokenDto(jwtToken));
    }

    // TODO ForgetPassword

}

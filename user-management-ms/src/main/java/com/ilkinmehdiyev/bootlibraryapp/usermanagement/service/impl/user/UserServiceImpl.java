package com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.impl.user;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.generics.EntityCouldNotSavedException;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.JwtService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.LoginDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.UserDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.EmailCouldNotSentException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.UserNotActiveException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.UserNotFoundException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.UsernameAlreadyExistException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.mapper.UserMapperImpl;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.ApplicationUser;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.Authority;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.repo.UserRepo;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.mail.MailService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.token.TokenService;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.utils.Constants.*;

@Slf4j
@Service
public record UserServiceImpl(
        UserRepo userRepo,
        TokenService tokenService,
        UserMapperImpl mapper,
        PasswordEncoder passwordEncoder,
        MailService mailService,
        JwtService jwtService,
        AuthenticationManagerBuilder authenticationManagerBuilder)
        implements UserService {

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.trace("Loading user by {} email", email);
//        return userRepo.findByUsername(username)
        return userRepo.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Cannot find user with " + email + " email."));
    }

    @Override
    public void signUp(UserDto userDto) {
        userRepo.findByUsername(userDto.getUsername())
                .ifPresent(user -> {
                    log.error("User with " + userDto.getUsername() + " already exists");
                    throw new UsernameAlreadyExistException(userDto.getUsername());
                });


        ApplicationUser applicationUser = mapper.toUser(userDto);
        applicationUser.setPassword(passwordEncoder.encode(userDto.getPassword()));


        Set<Authority> authorities = userDto.getAuthorities().stream()
                .map(authority -> new Authority(authority.getName(), Set.of(applicationUser))).collect(Collectors.toSet());
        applicationUser.setAuthorities(authorities);

        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(applicationUser);
        verificationToken.setToken(UUID.randomUUID().toString());
        verificationToken.setExpiresAt(LocalDateTime.now().plus(1, ChronoUnit.HOURS));
        applicationUser.setVerificationToken(verificationToken);

        try {
            userRepo.save(applicationUser);
            log.trace("Entity saved successfully: {}", applicationUser);
        } catch (Exception e) {
            log.error("Something happened, entity could not be saved");
            throw new EntityCouldNotSavedException(e);
        }

        try {
            mailService.sendVerificationMail(userDto.getEmail(), VERIFICATION_API + applicationUser.getVerificationToken().getToken());
            log.trace("Verification email successfully sent to {}", userDto.getEmail());
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error("Cannot send the verification email");
            throw new EmailCouldNotSentException(e.getMessage());
        }
    }

    @Override
    public void getAllUsers(String query, Pageable pageable) {

    }

    @Override
    public void activateUser(String token) {

        VerificationToken verificationToken = tokenService.confirmUserToken(token);

        ApplicationUser user = userRepo.findByVerificationToken(verificationToken).orElseThrow(() -> {
            log.error("User with {} token not found!", verificationToken.getToken());
            throw new UserNotFoundException();
        });

        user.setEnabled(true);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);

        userRepo.save(user);
    }

    @Override
    public String signIn(LoginDto loginDto) {
        log.trace("Login request by user {}", loginDto.email());
//        ApplicationUser user = userRepo.findByEmail(loginDto.email()).orElseThrow(() -> {
//            log.error("user with {} email not found", loginDto.email());
//            throw new UserNotFoundException();
//        });
        UserDetails user = this.loadUserByUsername(loginDto.email());

        if (!user.isEnabled() || !user.isAccountNonLocked())
            throw new UserNotActiveException();

        log.trace("Authenticating user {}", loginDto.email());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.email(), loginDto.password());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Duration duration = loginDto.rememberMe() ? REMEMBER_ONE_WEEK : REMEMBER_ONE_DAY;
        return jwtService.issueToken(authentication, duration);
    }
}

package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.NotFoundException;

public class TokenNotFoundException extends NotFoundException {
    public TokenNotFoundException(String message) {
        super(message);
    }
}

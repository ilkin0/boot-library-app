package com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.BaseEntity;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.user.ApplicationUser.TABLE_NAME;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = TABLE_NAME)
public class ApplicationUser extends BaseEntity implements UserDetails {

    public static final String TABLE_NAME = "users";

    @Column(unique = true)
    private String username;

    @Column(unique = true)
    private String email;

    private String password;

    private String name;

    private int age;

    private String avatar;

    @Column(updatable = false)
    private LocalDateTime creationDate;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private VerificationToken verificationToken;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;

    @JsonIgnore
    @Builder.Default
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "user_authority",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
    private Set<Authority> authorities = new HashSet<>();
}

package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class EmailCouldNotSentException extends InvalidStateException {

    public EmailCouldNotSentException(String message) {
        super(message);
    }
}

package com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.user;

import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.LoginDto;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.dto.UserDto;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    void signUp(UserDto userDto);

    void getAllUsers(String query, Pageable pageable);

    void activateUser(String token);

    String signIn(LoginDto loginDto);
}

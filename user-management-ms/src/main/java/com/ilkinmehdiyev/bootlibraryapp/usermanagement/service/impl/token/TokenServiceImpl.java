package com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.impl.token;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.TokenExpiredException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.TokenNotFoundException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception.UserAlreadyConfirmedException;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.model.token.VerificationToken;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.repo.TokenRepo;
import com.ilkinmehdiyev.bootlibraryapp.usermanagement.service.interfaces.token.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public record TokenServiceImpl(TokenRepo tokenRepo) implements TokenService {

    @Override
    public void saveVerificationToken(VerificationToken token) {
        tokenRepo.save(token);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return tokenRepo.findByToken(token)
                .orElseThrow(() -> {
                    log.error("Token {} not found!", token);
                    throw new TokenNotFoundException("Token not found");
                });
    }

    @Override
    public void changeTokenConfirmedAt(String token) {
        try {
            tokenRepo.updateTokenConfirmAt(token, LocalDateTime.now());
            log.trace("Verification token has been used!");
        } catch (Exception e) {
            log.error("Cannot update Verification token state", e);
            throw new InvalidStateException("Cannot update Verification token state");
        }
    }

    @Override
    public VerificationToken confirmUserToken(String token) {
        VerificationToken verificationToken = this.getVerificationToken(token);

        if (verificationToken.getConfirmedAt() != null)
            throw new UserAlreadyConfirmedException();

        LocalDateTime expiresAt = verificationToken.getExpiresAt();

        if (expiresAt.isBefore(LocalDateTime.now()))
            throw new TokenExpiredException();

        this.changeTokenConfirmedAt(token);
        return verificationToken;
    }
}

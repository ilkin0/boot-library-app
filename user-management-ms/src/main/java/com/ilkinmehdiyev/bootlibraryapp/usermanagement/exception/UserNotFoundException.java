package com.ilkinmehdiyev.bootlibraryapp.usermanagement.exception;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    public UserNotFoundException() {
        super("User not found!");
    }
}

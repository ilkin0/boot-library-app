package com.ilkinmehdiyev.bootlibraryapp.usermanagement.utils;

import lombok.Getter;
import lombok.Setter;

import java.time.Duration;

@Getter
@Setter
public class Constants {

    //    API endpoints
    public static final String API_URL = "/api/v1";
    public static final String USER_API_URL = API_URL + "/users";
    public static final String AUTH_API_URL = API_URL + "/auth";

    // String Constants
    public static final String VERIFICATION_API = "http://localhost:8081" + AUTH_API_URL + "/confirm?token=";
    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 50;
    public static final Duration REMEMBER_ONE_DAY = Duration.ofDays(1);
    public static final Duration REMEMBER_ONE_WEEK = Duration.ofDays(7);
}

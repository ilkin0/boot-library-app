# TODOs

### Registration

- [ ] Register the User
    - [ ] Implement User Authorities/Roles.
    - [ ] Implement JWT issue and verify.
    - [ ] Register the User to the DB.
    - [ ] Send email verification to the user email.


- [ ] Login the User
    - [ ] If the user enabled and jwt is OK, log the user


- [ ] Implement Custom Exception and GlobalExceptionHandling


- [ ] Book CRUDs
    - [ ] Add MinIO and S3 supports for the Book Covers
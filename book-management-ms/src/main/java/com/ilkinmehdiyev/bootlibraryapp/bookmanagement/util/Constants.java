package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String API_URL = "/api/v1";
    public static final String BOOK_API_URL = API_URL + "/books";
}

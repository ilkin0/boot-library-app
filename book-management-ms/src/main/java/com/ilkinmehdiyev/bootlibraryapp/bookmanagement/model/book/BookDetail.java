package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.BaseEntity;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@ToString
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = BookDetail.TABLE_NAME)
public class BookDetail extends BaseEntity {

    public static final String TABLE_NAME = "book_details";

    private String isbn;
    private LocalDateTime publishDate;
    private int rating;
    private int pageSize;
    private String cover;
    private String preview;
    private BigDecimal price;

    // TODO Implement Genre entity
    // TODO Implement Language entity

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(mappedBy = "detail", cascade = CascadeType.ALL)
    private Book book;
}

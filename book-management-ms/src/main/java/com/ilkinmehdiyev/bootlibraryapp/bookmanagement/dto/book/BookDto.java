package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.author.AuthorDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book.BookDetail;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.publisher.Publisher;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class BookDto {
    private String name;
    private Set<AuthorDto> authors = new HashSet<>();
    private BookDetail detail;
    private Publisher publisher;
}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.controller.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.book.BookDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.service.interfaces.book.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

import static com.ilkinmehdiyev.bootlibraryapp.bookmanagement.util.Constants.BOOK_API_URL;

@Slf4j
@RestController
@RequestMapping(BOOK_API_URL)
public record BookController(BookService bookService) {

    @GetMapping
    public Set<BookDto> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/{id}")
    public BookDto getBook(@PathVariable("id") Long bookId) {
        return bookService.getBookDtoById(bookId);
    }

    @PostMapping
    public ResponseEntity<?> saveBook(@Valid @RequestBody BookDto bookDto) {
        bookService.saveBook(bookDto);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateBook(@PathVariable("id") Long bookId, @RequestBody @Valid BookDto bookDto) {
        bookService.updateBook(bookDto, bookId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable("id") Long bookId){
        bookService.deleteBook(bookId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.mapper.author;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.author.AuthorDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.author.Author;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    AuthorDto toAuthorDto(Author author);

    Author toAuthor(AuthorDto authorDto);
}

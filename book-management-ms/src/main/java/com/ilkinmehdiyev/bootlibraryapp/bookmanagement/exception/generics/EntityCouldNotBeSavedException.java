package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.exception.generics;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class EntityCouldNotBeSavedException extends InvalidStateException {

    public EntityCouldNotBeSavedException() {
        super("Entity could not be saved!");
    }
}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.mapper.publisher;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.publisher.PublisherDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.publisher.Publisher;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PublisherMapper {
    PublisherDto toPublisherDto(Publisher publisher);
}

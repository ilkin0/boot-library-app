package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.service.implementation.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.book.BookDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.exception.generics.EntityCouldNotBeSavedException;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.mapper.author.AuthorMapper;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.mapper.book.BookMapper;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book.Book;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.repo.book.BookRepo;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.service.interfaces.book.BookService;
import com.ilkinmehdiyev.bootlibraryapp.common.exception.generics.EntityCouldNotDeletedException;
import com.ilkinmehdiyev.bootlibraryapp.common.exception.generics.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepo bookRepo;
    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;


    @Override
    public Set<BookDto> getAllBooks() {
        return bookRepo.findAll()
                .stream().map(bookMapper::toBookDto)
                .collect(Collectors.toSet());
    }


    @Override
    public Book getBookById(Long bookId) {
        return bookRepo.findBookById(bookId).orElseThrow(() -> {
            log.error("Book with {} id not found", bookId);
            throw new EntityNotFoundException("Book with " + bookId + " id not found");
        });
    }

    @Override
    @Transactional
    public BookDto getBookDtoById(Long bookId) {
        Book bookById = this.getBookById(bookId);
        return bookMapper
                .toBookDto(bookById);
    }

    @Override
    public void saveBook(BookDto bookDto) {
        Book book = getBookEntity(bookDto);

        try {
            bookRepo.save(book);
            log.trace("Book {} saved!", book);
        } catch (Exception e) {
            log.error("Cannot save book {}", book, e);
            throw new EntityCouldNotBeSavedException();
        }
    }

    @Override
    public void updateBook(BookDto bookDto, Long bookId) {

        Book newBook = getBookEntity(bookDto);
        Book book = this.getBookById(bookId);

        BeanUtils.copyProperties(newBook, book);
        book.setId(bookId);

        try {
            bookRepo.save(book);
            log.trace("Book {} saved!", book);
        } catch (Exception e) {
            log.error("Cannot save book {}", book, e);
            throw new EntityCouldNotBeSavedException();
        }
    }

    @Override
    public void deleteBook(Long bookId) {
        Book bookById = this.getBookById(bookId);

        try {
            bookRepo.delete(bookById);
            log.trace("Book has been deleted successfully!");
        } catch (Exception e) {
            log.error("Book cannot be deleted", e);
            throw new EntityCouldNotDeletedException(e);
        }
    }

    private Book getBookEntity(BookDto bookDto) {
        return Book.builder()
                .authors(bookDto.getAuthors()
                        .stream()
                        .map(authorMapper::toAuthor).collect(Collectors.toSet()))
                .detail(bookDto.getDetail())
                .name(bookDto.getName())
                .publisher(bookDto.getPublisher()).build();
    }
}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.author;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.book.BookDto;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class AuthorDto {
    private String name;
    private LocalDateTime birthDate;
    private String bio;
    private Set<BookDto> books = new HashSet<>();
}

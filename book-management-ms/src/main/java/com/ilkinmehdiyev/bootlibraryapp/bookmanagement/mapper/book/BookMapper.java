package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.mapper.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.book.BookDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface BookMapper {

    @Mapping(target = "authors", ignore = true)
        // TODO cyclic dto mapping make stackoverflow
    BookDto toBookDto(Book book);

    Book toBook(BookDto bookDto);
}

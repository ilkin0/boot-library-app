package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.publisher;

import lombok.Getter;

@Getter
public class PublisherDto {
    private String name;
}

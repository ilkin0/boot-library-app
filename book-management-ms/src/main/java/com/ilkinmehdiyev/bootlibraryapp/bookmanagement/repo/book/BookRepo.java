package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.repo.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book.Book;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookRepo extends JpaRepository<Book, Long> {

//    @Override
    @EntityGraph(attributePaths = {"authors", "detail", "publisher"})
    Optional<Book> findBookById(Long aLong);
}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.author;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.BaseEntity;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book.Book;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@ToString
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = Author.TABLE_NAME)
public class Author extends BaseEntity {
    public static final String TABLE_NAME = "authors";

    private String name;
    private LocalDateTime birthDate;
    private String bio;

    @Builder.Default
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "authors")
    private Set<Book> books = new HashSet<>();
}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.config;

import com.ilkinmehdiyev.bootlibraryapp.common.security.auth.AuthenticationEntryPointConfigurer;
import com.ilkinmehdiyev.bootlibraryapp.common.security.config.BaseSecurityConfig;
import com.ilkinmehdiyev.bootlibraryapp.common.security.config.properties.SecurityProperties;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.impl.JwtServiceImpl;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.AuthService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.List;

import static com.ilkinmehdiyev.bootlibraryapp.bookmanagement.util.Constants.BOOK_API_URL;

@Configuration
@Import({JwtServiceImpl.class, SecurityProperties.class, AuthenticationEntryPointConfigurer.class})
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends BaseSecurityConfig {

    public SecurityConfig(List<AuthService> authServices) {
        super(authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .authorizeRequests().antMatchers(HttpMethod.GET, BOOK_API_URL)
                .authorizeRequests().antMatchers(BOOK_API_URL + "/**")
                .permitAll()
//                .antMatchers(HttpMethod.POST, BOOK_API_URL).hasRole("PUBLISHER")
        ;

        super.configure(http);
    }
}

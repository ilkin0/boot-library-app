package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({})
public class ApplicationConfiguration {
}

package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.publisher;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@ToString
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = Publisher.TABLE_NAME)
public class Publisher extends BaseEntity {
    public static final String TABLE_NAME = "publishers";

    private String name;
}

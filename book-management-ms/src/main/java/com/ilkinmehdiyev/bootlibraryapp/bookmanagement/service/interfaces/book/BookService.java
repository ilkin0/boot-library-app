package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.service.interfaces.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.dto.book.BookDto;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book.Book;

import java.util.Set;

public interface BookService {

    Set<BookDto> getAllBooks();

    Book getBookById(Long bookId);

    BookDto getBookDtoById(Long bookId);

    void saveBook(BookDto bookDto);

    void updateBook(BookDto bookDto, Long bookId);

    void deleteBook(Long bookId);
}

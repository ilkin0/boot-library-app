package com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.book;

import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.BaseEntity;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.author.Author;
import com.ilkinmehdiyev.bootlibraryapp.bookmanagement.model.publisher.Publisher;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@ToString
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = Book.TABLE_NAME)
public class Book extends BaseEntity {
    public static final String TABLE_NAME = "books";

    private String name;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "detail_id", referencedColumnName = "id")
    private BookDetail detail;

    @Builder.Default
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "book_author",
            joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"))
    private Set<Author> authors = new HashSet<>();


    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;
}

package com.ilkinmehdiyev.bootlibraryapp.common.exception.generics;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class EntityCouldNotDeletedException extends InvalidStateException {
    public static final String MESSAGE = "Entity could not be deleted";

    public EntityCouldNotDeletedException(Throwable cause) {
        super(MESSAGE, cause);
    }


}

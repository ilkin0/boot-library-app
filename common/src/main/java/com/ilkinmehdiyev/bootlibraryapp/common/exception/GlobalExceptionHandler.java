package com.ilkinmehdiyev.bootlibraryapp.common.exception;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.UUID.randomUUID;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(InvalidStateException.class)
    public final ResponseEntity<ErrorContainer> handleCouldNotBeDeleted(InvalidStateException exception) {
        ResponseEntity<ErrorContainer> exceptionFormat = getErrorContainer(exception.getStatus(), List.of(exception.getMessage()));
        log.trace("Request is invalid state {}", exception.getMessage());
        return exceptionFormat;
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ErrorContainer> handleCouldNotBeDeleted(NotFoundException exception) {
        ResponseEntity<ErrorContainer> exceptionFormat = getErrorContainer(exception.getStatus(), List.of(exception.getMessage()));
        log.trace("Entity not found {}", exception.getMessage());
        return exceptionFormat;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<ErrorContainer> handleCouldNotBeDeleted(ConstraintViolationException exception) {
        ResponseEntity<ErrorContainer> exceptionFormat = getErrorContainer(HttpStatus.BAD_REQUEST, getConstraintViolationExceptionMessage(exception));
        log.trace("Entity not found {}", exception.getMessage());
        return exceptionFormat;
    }

    private ResponseEntity<ErrorContainer> getErrorContainer(HttpStatus status, List<String> message) {
        ErrorContainer errorContainer = ErrorContainer.builder()
                .timestamp(System.currentTimeMillis())
                .date(LocalDateTime.now()
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .details(status)
                .errors(message)
                .errorId(randomUUID())
                .build();

        return new ResponseEntity<>(errorContainer, errorContainer.getDetails());
    }

    private List<String> getConstraintViolationExceptionMessage(ConstraintViolationException ex) {
        return ex.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
    }
}

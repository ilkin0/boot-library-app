package com.ilkinmehdiyev.bootlibraryapp.common.exception.generics;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class EntityCouldNotSavedException extends InvalidStateException {
    public static final String MESSAGE = "Entity could not be saved to DB";

    public EntityCouldNotSavedException(Throwable cause) {
        super(MESSAGE, cause);
    }


}

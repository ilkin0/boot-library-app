package com.ilkinmehdiyev.bootlibraryapp.common.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class ErrorContainer {
    private long timestamp;
    private String date;
    private HttpStatus details;
    private List<String> errors;
    private UUID errorId;
}
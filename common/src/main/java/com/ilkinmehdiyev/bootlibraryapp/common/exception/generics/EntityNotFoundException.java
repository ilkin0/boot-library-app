package com.ilkinmehdiyev.bootlibraryapp.common.exception.generics;

import com.ilkinmehdiyev.bootlibraryapp.common.exception.InvalidStateException;

public class EntityNotFoundException extends InvalidStateException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}

package com.ilkinmehdiyev.bootlibraryapp.common.security.auth;

import com.ilkinmehdiyev.bootlibraryapp.common.security.auth.filter.JwtAuthorizationFilter;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class JwtAuthFilterConfigurerAdapter extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final List<AuthService> authServices;

    @Override
    public void configure(HttpSecurity builder) {
        log.trace("Added auth request filter");
        builder.addFilterBefore(new JwtAuthorizationFilter(authServices), UsernamePasswordAuthenticationFilter.class);
    }
}

package com.ilkinmehdiyev.bootlibraryapp.common.security;

public enum UserAuthority {
    ANONYMOUS,
    USER,
    ADMIN,
    PUBLISHER;

}

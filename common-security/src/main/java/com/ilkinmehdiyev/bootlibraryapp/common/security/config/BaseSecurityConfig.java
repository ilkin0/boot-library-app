package com.ilkinmehdiyev.bootlibraryapp.common.security.config;

import com.ilkinmehdiyev.bootlibraryapp.common.security.auth.JwtAuthFilterConfigurerAdapter;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.util.List;

import static com.ilkinmehdiyev.bootlibraryapp.common.security.util.Constants.*;

@RequiredArgsConstructor
public abstract class BaseSecurityConfig extends WebSecurityConfigurerAdapter {

    private final List<AuthService> authServices;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(r -> r
                        .antMatchers("/", "/error").permitAll()
                        .antMatchers("/user/*").permitAll()
                        .antMatchers(ACTUATOR).permitAll()
                        .antMatchers(SWAGGER2).permitAll()
                        .antMatchers(SWAGGER3).permitAll()
                        .antMatchers(SWAGGER_HTML).permitAll()
                        .antMatchers(SWAGGER_UI).permitAll()
                        .anyRequest().authenticated()
                )
                .exceptionHandling(e -> e

                        .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)))
                .csrf(c -> c
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()))
                .csrf().disable()
                .cors().disable()
                .logout(l -> l
                        .logoutSuccessUrl("/").permitAll());
        // Session management
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().exceptionHandling().authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));

        // Apply filters
        http.apply(new JwtAuthFilterConfigurerAdapter(authServices));
        //Disallow all requests by default unless explicitly defined in submodules
//        http.authorizeRequests().anyRequest().access(authorities(SUPER_USER.name(), ADMIN.name()));
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs", "/v3/api-docs", "/configuration/ui",
                "/swagger-resources/**", "/configuration/**", "/swagger-ui.html",
                "/webjars/**", "/csrf", "/");
    }
}

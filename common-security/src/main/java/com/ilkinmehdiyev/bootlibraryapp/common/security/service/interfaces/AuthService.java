package com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public interface AuthService {

    Optional<Authentication> getAuthentication(HttpServletRequest request);
}

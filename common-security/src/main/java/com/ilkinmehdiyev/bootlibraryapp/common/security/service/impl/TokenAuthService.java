package com.ilkinmehdiyev.bootlibraryapp.common.security.service.impl;

import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.AuthService;
import com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces.JwtService;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.ilkinmehdiyev.bootlibraryapp.common.utils.HttpConstants.AUTH_HEADER;
import static com.ilkinmehdiyev.bootlibraryapp.common.utils.HttpConstants.BEARER_AUTH_HEADER;

@Slf4j
@Service
public record TokenAuthService(
        JwtService jwtService) implements AuthService {

    private static final String ROLE_CLAIMS = "role";

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(AUTH_HEADER))
                .filter(this::isBearAuth)
                .flatMap(this::getAuthenticationBearer);
    }

    private boolean isBearAuth(String header) {
        return header.toLowerCase().startsWith(BEARER_AUTH_HEADER.toLowerCase());
    }

    private Optional<Authentication> getAuthenticationBearer(String header) {
        String token = header.substring(BEARER_AUTH_HEADER.length()).trim();
        Claims claims = jwtService.parseToken(token);
        log.trace("The {} claims hase been parsed", claims);

        if (claims.getExpiration().before(new Date()))
            return Optional.empty();

        return Optional.of(getAuthenticationBearer(claims));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> roles = claims.get(ROLE_CLAIMS, List.class);

        List<SimpleGrantedAuthority> authorities = roles.stream()
                .map(r -> new SimpleGrantedAuthority(r.toString()))
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
    }
}

package com.ilkinmehdiyev.bootlibraryapp.common.security.util;

import lombok.Getter;

@Getter
public class Constants {
    public static final String ACTUATOR = "/actuator/**";
    public static final String SWAGGER2 = "/v2/api-docs";
    public static final String SWAGGER3 = "/v3/api-docs";
    public static final String SWAGGER_UI = "/swagger-ui/**";
    public static final String SWAGGER_HTML = "/swagger-ui.html";
}

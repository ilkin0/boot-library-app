package com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces;

import java.util.Optional;

public interface SecurityService {
    Optional<String> getCurrentUserLogin();
}

package com.ilkinmehdiyev.bootlibraryapp.common.security.service.interfaces;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.Authentication;

import java.time.Duration;

public interface JwtService {
    String issueToken(Authentication authentication, Duration duration);

    boolean isTokenValid(String token);

    String getEmailFromToken(String token);

    Claims parseToken(String token);
}
